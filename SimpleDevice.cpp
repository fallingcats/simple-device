//#include "settings.h"
#include "SimpleDevice.h"

SimpleDevice::SimpleDevice() {
}
void SimpleDevice::begin(SimpleConfig config) {
    begin(config, false);
}
void SimpleDevice::begin(SimpleConfig config, bool debug) {
    using namespace std::placeholders;
    _config = config;
    _debug = debug;

    if(_debug)
        Serial.begin(115200);

    debug_print("\n\ncompiled on " __DATE__ " @ " __TIME__);

    if(_config.use_led_indicator)
        pinMode(LED_BUILTIN, OUTPUT);

    WiFi.mode(WIFI_STA);
    WiFi.begin(_config.wifi_ssid, _config.wifi_pass);
    while (WiFi.status() != WL_CONNECTED) {
        delay(200);
        debug_print("!");
        
        if(_config.non_blocking_reconnect)
            break;
    }
    debug_print(WiFi.localIP().toString());

    _base_topic = _config.device_type + String("/") + _config.device_name + "/";
    _topic_command = _base_topic;
    _topic_available = _base_topic + "available";
    _topic_state = _base_topic + "state";
    _topic_verbose = _base_topic + "verbose";

    _mqtt.setClient(_tcpClient);
    _mqtt.setServer(_config.mqtt_host, _config.mqtt_port);
    _mqtt.setCallback(std::bind(&SimpleDevice::_callback_hook, this, _1, _2, _3));
}


void SimpleDevice::loop() {
    while (!_mqtt.connected()) {
        debug_print(".");
        if (_mqtt.connect(_config.device_name, _config.mqtt_user, _config.mqtt_pass, _topic_available.c_str(), 1, true, _payload_availability[false].c_str())) {
            debug_print("connected");
            _mqtt.subscribe(_topic_command.c_str());
            for(int i = 0; i < 10; i++) {
                if(!_topics_subscribe[i].length())
                    break;
                _mqtt.subscribe(_topics_subscribe[i].c_str());
            }
            _mqtt.publish(_topic_available.c_str(), _payload_availability[true].c_str(), true);
            _mqtt.publish(_topic_state.c_str(), _payload_state[_state].c_str(), _config.mqtt_retain);
        }

        if(_config.non_blocking_reconnect)
            break;

        if(!_mqtt.connected())
            delay(1000);
    }
    _mqtt.loop();
}


void SimpleDevice::setState(bool state) {
    _state = state;
    if(_pin >= 0){
        digitalWrite(_pin, _state ^ _pinInvertedLogic);
        debug_print("Switching pin "+ String(_pin) +" to " + String(_state ^ _pinInvertedLogic ? "LOW":"HIGH") + " ("+String(_state ? "false":"true")+")");
    }
    if(_callback_setstate) {
        _callback_setstate(_state);
    }


    if(_config.use_led_indicator)
        digitalWrite(LED_BUILTIN, !_state);

    _mqtt.publish(_topic_state.c_str(), _payload_state[_state].c_str(), _config.mqtt_retain);
    debug_print(String("new logic state: ")+ String(_state));
}

bool SimpleDevice::getState() {
    return _state;
}

String SimpleDevice::getDeviceTopic(){
    return _base_topic;
}
String SimpleDevice::getDeviceTopic(String subTopic){
    return getDeviceTopic() + subTopic;
}


void SimpleDevice::setPin(int pin, bool invertedLogic) {
    // invertedLogic == true  means setState(false) turns the pin HIGH
    if(pin >= 0) {
        pinMode(pin, OUTPUT);
    }
    _pinInvertedLogic = invertedLogic;
    _pin = pin;
}
void SimpleDevice::setPin(int pin) {
    setPin(pin, false);
}

void SimpleDevice::clearPin() {
    // Negaqtive pin values disable setting a pin;
    _pin = -1;
}


void SimpleDevice::enableSwitchMode() {
    _switchCallback = true;
}
void SimpleDevice::enableSwitchMode(void (*callback_setstate)(bool state)) {
    enableSwitchMode();
    _callback_setstate = callback_setstate;
}
void SimpleDevice::enableSwitchMode(String universal_payload[], void (*callback_setstate)(bool state)) {
    enableSwitchMode(universal_payload, universal_payload, callback_setstate);
}
void SimpleDevice::enableSwitchMode(String payload_receive[], String payload_send[], void (*callback_setstate)(bool state)) {
    enableSwitchMode(callback_setstate);
    _payload_command[0] = payload_receive[0];
    _payload_command[1] = payload_receive[1];
    _payload_state[0] = payload_send[0];
    _payload_state[1] = payload_send[1];
    //_payload_state = payload_send;
}
void SimpleDevice::enableSwitchMode(String universal_payload[]) {
    enableSwitchMode(universal_payload, (void (*)(bool)) NULL);
}
void SimpleDevice::enableSwitchMode(String payload_receive[], String payload_send[]) {
    enableSwitchMode(payload_receive, payload_send, (void (*)(bool)) NULL);
}


void SimpleDevice::setCustomCallback(void (*cb)(String topic, String message)) {
    _callback_custom = cb;
}

void SimpleDevice::_callback_hook(char recvTopic[], byte recvPayload[], unsigned int recvLength) {
    String message = "";
    String topic = String(recvTopic);

    for(uint8_t i=0; i < recvLength; i++)
    message += (char) recvPayload[i];

    if(_switchCallback) {
        if (topic == _topic_command) {
            if (message == _payload_command[false]) {
                setState(false);
            } else if (message == _payload_command[true]) {
                setState(true);
            } else {
                debug_print("Unknown payload["+String(message.length())+"]: '" + message +"'");
            }
        }
    }
    if(_callback_custom) {
        _callback_custom(topic, message);
    }
}

void SimpleDevice::subscribeAdditionalTopic(String topic, bool useDeviceTopic) {
    for(int i = 0; i < 10; i++) {
        if(!_topics_subscribe[i].length()) {
            _topics_subscribe[i] = useDeviceTopic ? getDeviceTopic(topic) : topic;
            break;
        }
    }
}
void SimpleDevice::subscribeAdditionalTopic(String topic) {
    subscribeAdditionalTopic(topic, true);
}
void SimpleDevice::publish(String topic, String message){
    _mqtt.publish(topic.c_str(), message.c_str(), _config.mqtt_retain);
}


void SimpleDevice::debug_print(const char message[]) {
    if(_debug) {
        Serial.println(message);
        _mqtt.publish(_topic_verbose.c_str(), message);
    }
}
void SimpleDevice::debug_print(String message) {
    debug_print(message.c_str());
}
