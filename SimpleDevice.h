#include "Arduino.h"
#include <ESP8266WiFi.h>
#define MQTT_MAX_PACKET_SIZE 256
#include <PubSubClient.h>


struct SimpleConfig {
    const char *wifi_ssid;
    const char *wifi_pass;

    const char *mqtt_host;
    int         mqtt_port;
    const char *mqtt_user;
    const char *mqtt_pass;

    const char *device_type;
    const char *device_name;
    
    bool        mqtt_retain;
    bool        non_blocking_reconnect;
    bool        use_led_indicator;
};


class SimpleDevice {
public:
    SimpleDevice();
    void begin(SimpleConfig config);
    void begin(SimpleConfig config, bool debug);
    void loop();

    void setState(bool state);
    bool getState();

    String getDeviceTopic();
    String getDeviceTopic(String subTopic);

    void setPin(int pin);
    void setPin(int pin, bool invertedLogic);
    void clearPin();

    void enableSwitchMode();
    void enableSwitchMode(void (*callback_setstate)(bool state));
    void enableSwitchMode(String universal_payload[], void (*callback_setstate)(bool state));
    void enableSwitchMode(String payload_receive[], String payload_send[], void (*callback_setstate)(bool state));
    void enableSwitchMode(String universal_payload[]);
    void enableSwitchMode(String payload_receive[], String payload_send[]);
    void setCustomCallback(void (*cb)(String topic, String message));

    void subscribeAdditionalTopic(String topic, bool useDeviceTopic);
    void subscribeAdditionalTopic(String topic);
    void publish(String topic, String message);

    void debug_print(String message);
    void debug_print(const char message[]);

private:
    PubSubClient _mqtt;
    bool _state = false;
    bool _debug = false;

    int _pin = -1;
    bool _pinInvertedLogic = false;
    bool _switchCallback = false;

    String _payload_command[2] = {"OFF", "ON"};
    String _payload_state[2] = {"OFF", "ON"};
    String _payload_availability[2] = {"offline", "online"};

    String _base_topic;

    String _topic_command;
    String _topic_available;
    String _topic_state;
    String _topic_verbose;
    String _topics_subscribe[10] = {""};

    WiFiClient _tcpClient;
    void _callback_hook(char recvTopic[], byte recvPayload[], unsigned int recvLength);
    void (*_callback_custom)(String topic, String message) = NULL;
    void (*_callback_setstate)(bool state) = NULL;
    SimpleConfig _config;
};
