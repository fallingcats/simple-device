#include "SimpleDevice.h"

SimpleConfig config {
    .wifi_ssid = "wifi_ssid",
    .wifi_pass = "wifi_pass",

    .mqtt_host = "10.0.0.10",
    .mqtt_port = 1883,
    .mqtt_user = "mqtt_user, or remove this line",
    .mqtt_pass = "mqtt_pass, or remove this line",

    .device_type = "testing",
    .device_name = "simpledevice1",

    .non_blocking_reconnect = false
};

#define GPIO_BUTTON 0
#define GPIO_RELAY D6

SimpleDevice sd;
String payload[] = {"LOCK", "UNLOCK"};

void setState(bool state) {
    pinMode(GPIO_RELAY, state?OUTPUT:INPUT);
}

void setup() {
    Serial.begin(115200);
    sd.begin(config, true);
    sd.useSwitchCallback(payload, setState);
    sd.setState(false);
}

void loop() {
    sd.loop();
    if(!digitalRead(GPIO_BUTTON)) {
        Serial.print("_");
        sd.setState(!sd.getState());
        while(!digitalRead(GPIO_BUTTON)) {
            delay(20);
            sd.loop();
            yield();
        }
    }
}
