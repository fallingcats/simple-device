# SimpleDevice

Simplify implementing a binary switch or button with mqtt connectivity.

Please look into `examples/` to get an idea on how to use this.
